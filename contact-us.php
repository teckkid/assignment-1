<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Aleem Ahmad</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <body>
      <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="container-fluid ">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">Aleem Ahmad</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-right" id="navbarCollapse">
               <ul class="nav navbar-nav">
                  <li class="active"><a href="" target="_blank">Home</a></li>
                  <li><a href="/contact-us.php" target="_blank">Contact</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container-fluid">
		 <div class="row">
			<div class="col-md-12">
					<form role="form" id="contact-form" class="contact-form">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<h2>Contact us</h2>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="E-mail">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="Message"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<button type="submit" class="btn main-btn pull-right">Send a message</button>
							</div>
						</div>
					</form>
			</div>
		 </div>
         <hr>
         <div class="row">
            <div class="col-xs-12">
               <footer>
                  <p>&copy; Copyright 2013 Aleem Ahmad</p>
               </footer>
            </div>
         </div>
      </div>
   </body>
</html>