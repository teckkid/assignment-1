<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Aleem Ahmad</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="style.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   </head>
   <body>
      <nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="container-fluid ">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="index.php">Aleem Ahmad</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-right" id="navbarCollapse">
               <ul class="nav navbar-nav">
                  <li class="active"><a href="index.php">Home</a></li>
                  <li><a href="contact-us.php" >Contact</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container-fluid">
		 <div class="row">
			<div class="col-md-12">
					<div id="mainArea" >
						<section>
							<article>
								<div class="sectionTitle">
									<h1>Personal Profile</h1>
								</div>
								
								<div class="sectionContent">
									<p>To work in a challenging environment where I can use my skills in best way possible for achieving the company’s goals. </p>
								</div>
							</article>
							<div class="clear"></div>
						</section>
						
						
						<section>
							<div class="sectionTitle">
								<h1>Work Experience</h1>
							</div>
							
							<div class="sectionContent">
								<article>
									<h2>Descon Chemicals Private Limited​. </h2>
									<p class="subDetails">June 2018 - Sep 2018</p>
									<p>Network  Management <br> Computer maintenance <br> Server management <br> Data Centre maintenance <br> User support </p>
								</article>
								
								
							</div>
							<div class="clear"></div>
						</section>
						
						
						<section>
							<div class="sectionTitle">
								<h1>Key Skills</h1>
							</div>
							
							<div class="sectionContent">
								<ul class="keySkills">
									<li>Computer Operation</li>
									<li>Active Directories </li>
									<li>DHCP</li>
									<li>Server Configurations</li>
									<li>Users support</li>
									<li>User handling</li>
									<li>Server Management</li>
								</ul>
							</div>
							<div class="clear"></div>
						</section>
						
						
						<section>
							<div class="sectionTitle">
								<h1>Education</h1>
							</div>
							
							<div class="sectionContent">
								<article>
									<h2> Punjab University </h2>
									<p class="subDetails">Bachelors of  Information and Technology</p>
									<p>Introduction to Programming <br> Project  Management <br> Information  systems <br> Database  Management <br> Database  Administration</p>
								</article>
								
								<article>
									<h2>Govt. College of Science, Wahdat Rd. Lahore </h2>
									<p class="subDetails">Intermediate in Pre-Engineering </p>
									<p> Mathematics <br> Physics <br> Chemistry </p>
								</article>
							</div>
							<div class="clear"></div>
						</section>
						
					</div>
			</div>
		 </div>
         <hr>
         <div class="row">
            <div class="col-xs-12">
               <footer>
                  <p>&copy; Copyright 2013 Aleem Ahmad</p>
               </footer>
            </div>
         </div>
      </div>
   </body>
</html>